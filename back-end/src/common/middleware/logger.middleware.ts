import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: Function) {
    console.log('# Request ' , req.baseUrl);
    console.log(req.query)
    console.log(req.body)

    next();
  }
}