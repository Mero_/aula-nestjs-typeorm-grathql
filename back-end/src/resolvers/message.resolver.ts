import { Args, Mutation, Parent, Query, ResolveProperty, Resolver, ResolveField } from '@nestjs/graphql';
import RepoService from '../repo.service';
import User from '../db/models/user.entity';
import Message from '../db/models/message.entity';
import MessageInput from './input/message.input'

@Resolver(() => Message)
class MessageResolver {

  constructor(private readonly repoService: RepoService) {}

  @Query(() => Message, {nullable: true})
  public async getMessage(@Args('id') id: number): Promise<Message> {
    return await this.repoService.messageRepo.findOne(id);
  }

  @Query(() => [Message])
  public async getMessages(): Promise<Message[]> {
    return this.repoService.messageRepo.find();
  }
  
  @Query(() => [Message])
  public async getMessagesFromUser(
    @Args('userId') userId: number,
  ): Promise<Message[]> {
    return this.repoService.messageRepo.find({
      where: { userId },
    });
  }

  @Mutation(() => Message)
  public async createMessage(@Args('data') input: MessageInput): Promise<Message> {
    /*const bookGenres = await this.repoService.messageRepo.find({
        where: {genreId: parent.id},
        relations: ['book']
    });*/

    const message = await this.repoService.messageRepo.create({
      content: input.content,
      userId: input.userId,
    });

    return await this.repoService.messageRepo.save(message)
  }

  @ResolveField(() => User, { name: 'user' })
  public async getUser(@Parent() parent: Message): Promise<User> {
    return await this.repoService.userRepo.findOne(parent.userId);
  }

  /*@ResolveProperty()
  public async createMessage(@Parent() parent): Promise<Book[]> {
    const bookGenres = await this.repoService.bookGenreRepo.find({where: 
    {genreId: parent.id}, relations: ['book']});
    const books: Book[] = [];
    bookGenres.forEach(async bookGenre => books.push(await 
      bookGenre.book));
    return books;
  }*/
}

export default MessageResolver;