// Resolvers e inputs são utilizados para a integração com graphql
// não são necessários caso não vá utilizar graphql
// Resolvers são usados para trazer e alterar dados

import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import RepoService from '../repo.service'
import User from '../db/models/user.entity'
import UserInput from './input/user.input';

@Resolver(() => User)
class UserResolver {
   constructor(private readonly repoService: RepoService) {}

  @Query(() => [User])
  public async getUsers(): Promise<User[]> {
    return await this.repoService.userRepo.find();
  }
  @Query(() => User, {nullable: true})
  public async getUser(@Args('id') id: number): Promise<User> {
    return await this.repoService.userRepo.findOne(id);
  }

  @Mutation(() => User)
  public async createUser(@Args('data') input: UserInput): 
    Promise<User> {
      const user = this.repoService.userRepo.create({email: input.email});
      return await this.repoService.userRepo.save(user);
  }

  @Mutation(() => User)
  public async createOrLoginUser(@Args('data') input: UserInput): 
    Promise<User> {
      let user = await this.repoService.userRepo.findOne({
        where: {
          email: input.email
        }
      });

      if(!user) {
        user = this.repoService.userRepo.create({email: input.email});

        await this.repoService.userRepo.save(user);
      }

      return user
  }
}

export default UserResolver;