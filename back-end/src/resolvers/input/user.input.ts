// Inputs são utilizados para a integração com graphql
// não são necessários caso não vá utilizar graphql
// Inputs servem para inserir dados

import { Field, InputType } from '@nestjs/graphql';

@InputType()
class UserInput {
  @Field()
  readonly email: string;
}

export default UserInput;