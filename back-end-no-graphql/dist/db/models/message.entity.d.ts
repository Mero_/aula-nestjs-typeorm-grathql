import User from './user.entity';
export default class Book {
    id: number;
    title: string;
    userId: number;
    createdAt: Date;
    updatedAt: Date;
    user: User;
    userConnection: Promise<User>;
}
