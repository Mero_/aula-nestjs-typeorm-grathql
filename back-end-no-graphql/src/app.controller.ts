import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import RepoService from './repo.service';

@Controller()
export class AppController {
  constructor(private readonly repoService: RepoService) {}

  @Get('/')
  async getHello(): Promise<string> {
    const count = await this.repoService.messageRepo.count()

    return `Quantidade de mensagens: ${count}`
  }
}
